Description
===========

ウクレレのコード変換ライブラリです。

Usage
=====

```
import UkuleleChordConverter

original = "C"
after = "F"

transrator = UkuleleChordConverter.UkuleleChordConverter(original, after)

print transrator.convert("Am7")
print transrator.convert("C#dim")
```
