# -*- coding: utf-8 -*-
__author__ = 'hasegawa'

import sys

class UkuleleChordConverter(object):
  chord = {
    "C": 0,
    "C#": 1,
    "Db": 1,
    "D": 2,
    "D#": 3,
    "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6,
    "Gb": 6,
    "G": 7,
    "G#": 8,
    "Ab": 8,
    "A": 9,
    "A#": 10,
    "Bb": 10,
    "B": 11,
    "C": 12,
  }
  chordS = ("C","C#","D","D#","E", "F","F#","G","G#","A","A#","B", "C")
  chordF = ("C","Db","D","Eb","E", "F","Gb","G","Ab","A","Bb","B", "C")
  suffix = ("m7-5", "maj7", "sus4", "dim", "aug", "m7","m6", "6","7", "m", "9")

  def __init__(self, original, trans=None , diff=None ):
    self.original = original
    self.trans = trans
    self.diff = diff


    if self.chord.has_key(original) == False:
      raise NotChordException(original)

    if trans != None and self.chord.has_key(trans) == False:
        raise NotChordException(trans)

    if diff != None:
      if diff > 12:
        raise HanonOutOfRangeException(diff)

    if original != None and trans != None:
      self.diff = self.chord[trans] - self.chord[original]

  def convert(self, org):
    base = ''
    suf = ''

    if self.diff == None:
      raise NothingDiffException

    # コードの要素を分割
    try:
      if org[1] == "#" or org[1] == "b":
        base = org[:2]
        suf = org[2:]
      else:
        base = org[0]
        suf = org[1:]
    except IndexError:
      base = org[0]
      suf = org[1:]

    if self.chord.has_key(base) == False:
      raise NotChordException(base)

    if suf != '':
      try:
        self.suffix.index(suf)
      except:
        raise SuffixException(suf)

    if "b" in base:
      chord_list = self.chordF
    else:
      chord_list = self.chordS

    hanon = self.chord[base] + self.diff
    if hanon > 12:
      hanon = hanon%12

    return("{}{}".format(chord_list[hanon], suf))

class NothingDiffException(Exception):
  def __init__(self):
    pass

  def __str__(self):
    return "diff is not set."

class NotChordException(Exception):
  def __init__(self, str=None):
    self.errorChord = str

  def __str__(self):
    return "'{}' is not chord.".format(self.errorChord)

class HanonOutOfRangeException(Exception):
  def __init__(self, str=None):
    self.hanon = str

  def __str__(self):
    return "Hanonsu '{}' is out of range.".format(self.hanon)

class SuffixException(Exception):
  def __init__(self, suf=None):
    self.errorSuffix = suf

  def __str__(self):
    return "'' is not suffix.".format(self.errorSuffix)
