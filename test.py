# -*- coding: utf-8 -*-
from src import UkuleleChordConverter

__author__ = 'hasegawa'

import sys

org = sys.argv[1]
trans = sys.argv[2]

chord = ("C","C#","D","D#","E", "F","F#","G","G#","A","A#","B", "C")

transrator = UkuleleChordConverter.UkuleleChordConverter(org, trans)

for c in chord:
  print "{} -> {}".format(c, transrator.convert(c))
