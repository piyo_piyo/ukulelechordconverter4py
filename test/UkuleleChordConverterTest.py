# -*- coding: utf-8 -*-
__author__ = 'hasegawa'

import unittest,sys
import UkuleleChordConverter


class TestFunctions(unittest.TestCase):

  def setUp(self):
    pass

  def tearDown(self):
    pass

  # インスタンス作成
  def test1(self):
    beforekey = "C"
    afterkey = 'F'
    converter = UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey)
    self.assertEqual(converter.__class__.__name__, "UkuleleChordConverter" )

  # コードではない文字列をbeforekeyに入れる
  def test2(self):
    beforekey = "H"
    afterkey = 'F'

    self.assertRaises(UkuleleChordConverter.NotChordException, lambda :UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey))

# コードではない文字列をafterkeyに入れる
  def test3(self):
    beforekey = "F"
    afterkey = 'Q'

    self.assertRaises(UkuleleChordConverter.NotChordException, lambda :UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey))

# 小文字コードをbeforekeyに入れる
  def test4(self):
    beforekey = "f"
    afterkey = 'C'

    self.assertRaises(UkuleleChordConverter.NotChordException, lambda :UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey))

# 小文字コードをafterkeyに入れる
  def test5(self):
    beforekey = "F"
    afterkey = 'c'

    self.assertRaises(UkuleleChordConverter.NotChordException, lambda :UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey))

# CをFに移調して、CがFになることを確認
  def test6(self):
    beforekey = "C"
    afterkey = 'F'
    chord = 'C'
    result = 'F'

    converter = UkuleleChordConverter.UkuleleChordConverter(beforekey, afterkey)
    self.assertEqual(converter.convert(chord), result)


if __name__ == '__main__':
  unittest.main()